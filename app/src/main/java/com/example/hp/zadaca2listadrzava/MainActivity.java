package com.example.hp.zadaca2listadrzava;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    LinearLayout linearLayout;
    RelativeLayout relativeLayout;
    RelativeLayout relativeInnerLayout;
    ListView listView;
    ImageView countryFlag;
    TextView countryName;
    TextView countyCapital;
    ArrayList<String> data;
    ArrayAdapter adapter;
    String[][] table;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayout = (LinearLayout)findViewById(R.id.linear_layout);
        relativeLayout = (RelativeLayout)findViewById(R.id.relative_main_lo);
        relativeInnerLayout = (RelativeLayout) findViewById(R.id.relative_inner_lo);
        listView = (ListView)findViewById(R.id.main_list);
        countryFlag = (ImageView)findViewById(R.id.country_flag);
        countryName = (TextView)findViewById(R.id.country_name_box);
        countyCapital = (TextView)findViewById(R.id.country_capital_box);

        /*relativeLayout.setEnabled(false);
        relativeInnerLayout.setEnabled(false);*/


        data = new ArrayList<>();
        table= new String[250][3]; // Country | Capital | ISO2

        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,data);


        DownloadClass downloadClass = new DownloadClass();

        try {
            downloadClass.execute("http://www.geognos.com/api/en/countries/info/all.json");
        }catch (Exception e){
            e.printStackTrace();
        }
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                countryName.setText(adapterView.getItemAtPosition(i).toString());
                countyCapital.setText(table[i][1]);

                Picasso.with(MainActivity.this)
                        .load("http://www.geognos.com/api/en/countries/flag/"+table[i][2]+".png")
                        .error(R.drawable.placeholder)
                        .placeholder(R.drawable.placeholder)
                        .into(countryFlag);

                relativeInnerLayout.animate().alpha(1);
                relativeLayout.animate().alpha(0.2f);

                listView.setEnabled(false);

            }
        });
    }

    public void close(View view) {
        relativeLayout.animate().alpha(0);
        relativeInnerLayout.animate().alpha(0);
        listView.setEnabled(true);
    }


    public class DownloadClass extends AsyncTask<String, Void, String>{


        @Override
        protected String doInBackground(String... strings) {

            URL url;
            HttpURLConnection httpURLConnection = null;

            try{

                url = new URL(strings[0]);

                httpURLConnection = (HttpURLConnection)url.openConnection();

                InputStream inputStream = httpURLConnection.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder total = new StringBuilder();

                String line;
                while ((line = reader.readLine()) != null) {
                    total.append(line);

                }

                Log.v("PROCITANO ",total.toString().substring(102000,total.toString().length()));
                return total.toString();

            }catch (Exception e){
                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                //JSONObject jsonObject = new JSONObject(s);
                //String string =jsonObject.getString("Results"); //Ne cita dobro
                //Log.v("string ",string);

                String[] countries = s.split("(?=\\{\"Name)");

                for( String co : countries)
                    Log.v("c",co);

                    Log.v("duzina niza countries",String.valueOf(countries.length));

                for(int i=1;i<countries.length;i++){ //0 ne treba

                    String countryName = countries[i].substring(8);
                    String[] temp = countryName.split(", \"Capital\": ");

                    countryName=temp[0];
                    table[i-1][0]=countryName;

                    if(!temp[1].matches("null(.*)")){
                        String[] temp1 = temp[1].split(", \"GeoPt\":");
                        String[] temp2 = temp1[0].split("\"Name\":");

                        table[i-1][1]=temp2[1];  //country capital
                    }else{
                        table[i-1][1]="No capital";
                    }


                    String[] temp3 = temp[1].split("\"iso2\": \"");
                    table[i-1][2]=temp3[1].substring(0,2);  //iso2


                    Log.v("Country: "+ table[i-1][0]," Capital: "+ table[i-1][1]+"Index"+table[i-1][2]);

                    adapter.add(countryName);
                    adapter.notifyDataSetChanged();

                }


            }catch (Exception e){
                e.printStackTrace();
                Log.v("ex","ex");
            }
        }
    }
}
